# QO2_tp

Term paper repository for Quantum Optics 2. 
Three level atom coupled to two field modes in thermal states. 
Exploration of different parameters and initial states.
The aim is to calculate the entropy exchange between the fields and the atom and try to understand it better.
Explore what happens when you combine a pure state and a thermal field.
