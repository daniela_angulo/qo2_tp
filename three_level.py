#this calculates the entropies for the tripartite system
from qutip import * 
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm,colors

def at_dm()

#Constants and interaction Hamiltonian
N=10
t_steps=200
tlist=np.linspace(0.0,200.0, t_steps)
n_oc_a=0.5
n_oc_b=0.5
g_a=0.05
g_b=0.05

#Field operators
a=tensor(identity(3),destroy(N),identity(N))
b=tensor(identity(3),identity(N),destroy(N))

#Atomic operators
S_23=tensor(basis(3,1)*basis(3,2).dag(),identity(N),identity(N))
S_13=tensor(basis(3,0)*basis(3,2).dag(),identity(N),identity(N))
S_11=tensor(basis(3,0)*basis(3,0).dag(),identity(N),identity(N))
S_22=tensor(basis(3,1)*basis(3,1).dag(),identity(N),identity(N))
S_33=tensor(basis(3,2)*basis(3,2).dag(),identity(N),identity(N))
#Interaction Hamiltonian
H=(g_a)*(a.dag()*S_23+a*S_23.dag())+(g_b)*(b.dag()*S_13+b*S_13.dag())

#Initial condition - density matrices
psi_a=basis(3,1)
rho_a_0=psi_a*psi_a.dag()
rho_fa_0=thermal_dm(N,n_oc_a)
rho_fb_0=thermal_dm(N,n_oc_b)
#rho_fa_0=coherent_dm(N,n_oc_a)
#rho_fb_0=coherent_dm(N,n_oc_b)
rho_0=tensor(rho_a_0,rho_fa_0,rho_fb_0)

#Time evolution
result=mesolve(H,rho_0,tlist,[],[])
rho_t=result.states

#Partial traces to find the atom and the field
rho_a_t=[]
rho_fa_t=[]
rho_fb_t=[]

for i in range(0,t_steps):
	rho_a_t.append(rho_t[i].ptrace(0))

for i in range(0,t_steps):
	rho_fa_t.append(rho_t[i].ptrace(1))

for i in range(0,t_steps):
	rho_fb_t.append(rho_t[i].ptrace(2))

#Population of states
s_i=[]
for i in range(0,t_steps):
	#s_i.append(np.real(rho_a_t[i][0,0]))
	s_i.append(np.real((rho_t[i]*S_11).tr()))

#Entropies
ent_s_t=[]
for i in range(0,t_steps):
	ent_s_t.append(entropy_vn(rho_t[i])-entropy_vn(rho_0))

ent_a_t=[]
for i in range(0,t_steps):
	ent_a_t.append(entropy_vn(rho_a_t[i]))

ent_fa_t=[]
for i in range(0,t_steps):
	ent_fa_t.append(entropy_vn(rho_fa_t[i])-entropy_vn(rho_fa_0))

ent_fb_t=[]
for i in range(0,t_steps):
	ent_fb_t.append(entropy_vn(rho_fb_t[i])-entropy_vn(rho_fb_0))

ent_tot_sub=[]
for i in range(0,t_steps):
	ent_tot_sub.append(ent_fb_t[i]+ent_fa_t[i]+ent_a_t[i])



#Plot results
plt.plot(tlist,ent_a_t,'b',tlist,ent_fa_t,'g',tlist,ent_fb_t,'y',tlist,ent_tot_sub,'r')
plt.show()


