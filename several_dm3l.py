from qutip import * 
import numpy as np
import matplotlib.pyplot as plt
from math import sqrt
from random import *

def triangle(points):
	x=[];y=[]
	while(len(x)<points):
		n3=uniform(-sqrt(3)/2,sqrt(3)/2)
		n8=uniform(-1,1/2)
		if(0<=n3**2+n8**2<=1 and 0<=2*n8**3-6*n3**2*n8+3*n3**2+3*n8**2<=1):
			x.append(n3);y.append(n8)
	return x,y	

#Constants and interaction Hamiltonian
N=6
t_steps=120
tlist=np.linspace(0.0,120.0, t_steps)
n_oc_a=0.1
n_oc_b=0.2
g_a=0.05
g_b=0.05

#Field operators
a=tensor(identity(3),destroy(N),identity(N))
b=tensor(identity(3),identity(N),destroy(N))

#Atomic operators
S_23=tensor(basis(3,1)*basis(3,2).dag(),identity(N),identity(N))
S_13=tensor(basis(3,0)*basis(3,2).dag(),identity(N),identity(N))
S_11=tensor(basis(3,0)*basis(3,0).dag(),identity(N),identity(N))
S_22=tensor(basis(3,1)*basis(3,1).dag(),identity(N),identity(N))
S_33=tensor(basis(3,2)*basis(3,2).dag(),identity(N),identity(N))

#Gellmann matrices 
lambda3=Qobj([[1,0,0],[0,-1,0],[0,0,0]])
lambda8=(1/sqrt(3))*Qobj([[1,0,0],[0,1,0],[0,0,-2]])

#Interaction Hamiltonian
H=(g_a)*(a.dag()*S_23+a*S_23.dag())+(g_b)*(b.dag()*S_13+b*S_13.dag())

#Initial condition - density matrices
rho_fa_0=thermal_dm(N,n_oc_a)
rho_fb_0=thermal_dm(N,n_oc_b)
rho_f_0=tensor(rho_fa_0,rho_fb_0)

num_mixed=3000	
n3,n8=triangle(num_mixed)

np.savetxt('n3.dat',n3)
np.savetxt('n8.dat',n8)

for j in range(0,num_mixed):
	rho_a_0=(1/3.)*(identity(3)+sqrt(3)*(n3[j]*lambda3+n8[j]*lambda8))
	rho_0=tensor(rho_a_0,rho_f_0)
	#Time evolution
	result=mesolve(H,rho_0,tlist,[],[])
	rho_t=result.states
	#Partial traces to find the atom and the field
	rho_a_t=[]
	for i in range(0,t_steps):
		rho_a_t.append(rho_t[i].ptrace(0))
	rho_f_t=[]
	for i in range(0,t_steps):
		rho_f_t.append(rho_t[i].ptrace([1,2]))
	ent_a_t=[]
	for i in range(0,t_steps):
		ent_a_t.append(entropy_vn(rho_a_t[i])-entropy_vn(rho_a_0))
	ent_f_t=[]
	for i in range(0,t_steps):
		ent_f_t.append(entropy_vn(rho_f_t[i])-entropy_vn(rho_f_0))
	data=np.array([tlist,ent_f_t,ent_a_t])
	np.savetxt('./data_ent/ent_mixdm'+str(j)+'.dat', data.T)

