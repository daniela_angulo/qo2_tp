from qutip import * 
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm,colors
from math import sqrt,sin,cos,pi

def at_dm(r,theta,phi):
	I=identity(2)
	S_x=r*sin(theta)*cos(phi)*sigmax()
	S_y=r*sin(theta)*sin(phi)*sigmay()
	S_z=r*cos(theta)*sigmaz()
	rho=(1/2.)*(I+S_x+S_y+S_z)
	return rho

#Constants
N=12
t_steps=400
tlist=np.linspace(0.0,400.0, t_steps)
n_oc=1.02
wa=0.5
g=0.05

#Interaction hamiltonian
a=tensor(identity(2),destroy(N))
sm=tensor(destroy(2),identity(N))
sz=tensor(sigmaz(), identity(N))
H0=wa*a.dag()*a+wa*sz
Hi=g*(a.dag()*sm+a*sm.dag())

#Initial conditions - density matrices

#Generating several density matrices
#rho_a_0=at_dm(sqrt(7/10.),0,pi)
#psi_a=basis(2,0)
#psi_a=maximally_mixed_dm(2)
#rho_a_0=psi_a*psi_a.dag()
rho_a_0=(2/3.)*basis(2,0)*basis(2,0).dag()+(1/3.)*basis(2,1)*basis(2,1).dag()
rho_f_0=thermal_dm(N,n_oc)
#rho_f_0=coherent_dm(N,n_oc)
rho_0=tensor(rho_a_0,rho_f_0)

#Time evolution
result=mesolve(Hi,rho_0,tlist,[],[])
rho_t=result.states

#Partial traces to find the atom and the field
rho_a_t=[]
rho_f_t=[]

for i in range(0,t_steps):
	rho_a_t.append(rho_t[i].ptrace(0))

for i in range(0,t_steps):
	rho_f_t.append(rho_t[i].ptrace(1))

#Population of the excited state 
rho_a_ee=[]
for i in range(0,t_steps):
	rho_a_ee.append(np.real(rho_a_t[i][1,1]))

#Entropy
ent_a_t=[]
ent_f_t=[]
ent_sum_sub=[]
for i in range(0,t_steps):
	ent_a_t.append(entropy_vn(rho_a_t[i])-entropy_vn(rho_a_0))

for i in range(0,t_steps):
	ent_f_t.append(entropy_vn(rho_f_t[i])-entropy_vn(rho_f_0))

for i in range(0,t_steps):
	ent_sum_sub.append(ent_f_t[i]+ent_a_t[i])

#Plot results
plt.plot(tlist,ent_f_t,'b',tlist,ent_a_t,'g',tlist,ent_sum_sub,'r')
#plt.plot(tlist,rho_a_ee,'b')
plt.show()
#data=np.array([tlist,ent_f_t,ent_a_t])
#np.savetxt('two_lev.dat', data.T)



