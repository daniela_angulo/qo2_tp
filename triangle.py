from qutip import * 
import numpy as np
import matplotlib.pyplot as plt
from math import sqrt
from random import *

def triangle(points):
	x=[];y=[]
	while(len(x)<points):
		n3=uniform(-sqrt(3)/2,sqrt(3)/2)
		n8=uniform(-1,1/2)
		if(0<=n3**2+n8**2<=1 and 0<=2*n8**3-6*n3**2*n8+3*n3**2+3*n8**2<=1):
			x.append(n3);y.append(n8)
	return x,y	
	

x,y=triangle(1000)

plt.plot(x,y,'g.')
plt.show()
