from qutip import * 
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm,colors
from math import sqrt

#Constants and interaction Hamiltonian
N=6
t_steps=240
tlist=np.linspace(0.0,240.0, t_steps)
n_oc_a=0.1
n_oc_b=0.2
g_a=0.05
g_b=0.05

#Field operators
a=tensor(identity(3),destroy(N),identity(N))
b=tensor(identity(3),identity(N),destroy(N))

#Atomic operators
S_23=tensor(basis(3,1)*basis(3,2).dag(),identity(N),identity(N))
S_13=tensor(basis(3,0)*basis(3,2).dag(),identity(N),identity(N))
S_11=tensor(basis(3,0)*basis(3,0).dag(),identity(N),identity(N))
S_22=tensor(basis(3,1)*basis(3,1).dag(),identity(N),identity(N))
S_33=tensor(basis(3,2)*basis(3,2).dag(),identity(N),identity(N))

#Interaction Hamiltonian
H=(g_a)*(a.dag()*S_23+a*S_23.dag())+(g_b)*(b.dag()*S_13+b*S_13.dag())

#Initial condition - density matrices

psi_a=basis(3,1)
#psi_a=maximally_mixed_dm(3)
rho_a_0=psi_a*psi_a.dag()
#rho_a_0=(1/3.)*(identity(3)+sqrt(3)*(n3*lambda3+n8*lambda8))
#rho_a_0=(1/3.)*basis(3,0)*basis(3,0).dag()+(11/18.)*basis(3,1)*basis(3,1).dag()+(1/18.)*basis(3,2)*basis(3,2).dag()
rho_fa_0=thermal_dm(N,n_oc_a)
rho_fb_0=thermal_dm(N,n_oc_b)
#rho_fa_0=coherent_dm(N,n_oc_a)
#rho_fb_0=coherent_dm(N,n_oc_b)
rho_f_0=tensor(rho_fa_0,rho_fb_0)
rho_0=tensor(rho_a_0,rho_f_0)

#Time evolution
result=mesolve(H,rho_0,tlist,[],[])
rho_t=result.states

#Partial traces to find the atom and the field
rho_a_t=[]
rho_f_t=[]

for i in range(0,t_steps):
	rho_a_t.append(rho_t[i].ptrace(0))

for i in range(0,t_steps):
	rho_f_t.append(rho_t[i].ptrace([1,2]))

#Population of states
s_1=[]
for i in range(0,t_steps):
	s_1.append(np.real((rho_t[i]*S_11).tr()))

s_2=[]
for i in range(0,t_steps):
	s_2.append(np.real((rho_t[i]*S_22).tr()))

s_3=[]
for i in range(0,t_steps):
	s_3.append(np.real((rho_t[i]*S_33).tr()))

#Plot results
#plt.plot(tlist,s_1,'b',tlist,s_2,'g',tlist,s_3,'y')
#plt.show()
print("s11",max(s_1),min(s_1),"s22",max(s_2),min(s_2),"s33",max(s_3),min(s_3))
data=np.array([tlist,s_1,s_2,s_3])
np.savetxt('populations_ground.dat', data.T)
