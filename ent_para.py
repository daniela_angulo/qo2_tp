import numpy as np
import matplotlib.pyplot as plt
from math import sqrt

P=[]
for j in range(0,3000):
	data=np.loadtxt('./data_ent/ent_mixdm'+str(j)+'.dat')
	at=[]
	f=[]
	Pi=[]	
	for k in range(0,120):
		f.append(data[[k],[1]])		
		at.append(data[[k],[2]])
	for i in range(0,119):
		delta_a=at[i+1]-at[i]
		delta_f=f[i+1]-f[i]
		if(abs(delta_a)>=abs(delta_f)):
			Pi.append(delta_f/delta_a)
		else:
			Pi.append(delta_a/delta_f)
	P.append(np.mean(Pi))

n3=np.loadtxt('n3.dat')
n8=np.loadtxt('n8.dat')
dat=np.array([n3,n8,P])
np.savetxt('P.dat', dat.T)
